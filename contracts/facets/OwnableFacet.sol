//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;

import "../libraries/LibAppStorage.sol";

contract OwnableFacet is Modifiers {
    function owner()
        internal
        view
        returns (address)
    {
        return LibDiamond.contractOwner();
    }

    function transferOwnership(address newOwner) internal virtual
    {
        return LibDiamond.setContractOwner(newOwner);
    }  
}
