//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;

import "@openzeppelin/contracts/interfaces/IERC20Metadata.sol";
import "../libraries/LibAppStorage.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract CoreFacet is Modifiers, ReentrancyGuard {
    using Address for address;
    using SafeERC20 for IERC20;
    using SafeMath for uint256;

    event BatchLock(address[] beneficiaries, uint256[] amounts);
    event Lock(address beneficiary, uint256 amount);
    event Release(address indexed beneficiary, uint256 amount);
    event OwnerWithdraw(address indexed beneficiary);
    event Edit(address indexed beneficiary, uint256 amount);


    function lockAddress(address user) public view returns (Vesting memory) {
        return s._vestings[user];
    }

    function remaining(address user) public view returns (uint256) {
        return s._vestings[user].total - s._vestings[user].released;
    }

    function released(address user) public view returns (uint256) {
        return s._vestings[user].released;
    }

    function token() external view returns (address) {
        return address(s._erc20Token);
    }

    function tgeTimestamp() public view returns (uint256) {
        return s._tgeTimestamp;
    }

    function vestingStartTimestamp() external view returns (uint256) {
        return s._vestingStart;
    }

    function percentForTGE() external view returns (uint32) {
        return s._percentForTGE;
    }

    function percentForVesting() external view returns (uint32) {
        return s._percentForVesting;
    }

    function totalRound() external view returns (uint) {
        return s._totalRound;
    }

    function timePerRound() external view returns (uint256) {
        return s._timePerRound;
    }

    function currentRound() public view returns (uint) {
        uint64 timePassed = uint64((block.timestamp) - s._vestingStart);
        uint current = uint(timePassed / s._timePerRound);
        return current <= s._totalRound ? current : s._totalRound;
    }

    function currentReleasable() external view returns(uint256){
        return s._claimable;
    }

    function total() external view returns(uint256){
        return s._total;
    }

    function blocktime() external view returns(uint256){
        return block.timestamp;
    }

    // batch lock token for many addresses in one transaction
    // the amount will be vested will follow the equation : vested = amount - immedialyReleased
    function batchLock(
        address[] calldata beneficiaries,
        uint256[] calldata amounts
    ) external onlyOwner nonReentrant {
        require(
            beneficiaries.length == amounts.length ,"Bizverse: Inputs not valid"
        );
        uint8 length = uint8(beneficiaries.length);
        uint256 totalAmount = 0;
        for (uint i = 0; i < length; i++) {
            require(
                beneficiaries[i] != address(0),
                "Bizverse: Can not batch lock to black hole"
            );
            s._vestings[beneficiaries[i]] = Vesting(amounts[i], 0);
            totalAmount += amounts[i];
        }
        s._total += totalAmount;
        emit BatchLock(beneficiaries, amounts);
    }

    function changeWallet(address _old, address _new) external onlyOwner{
        s._vestings[_new] = Vesting(s._vestings[_old].total, 0);
        emit Lock(_new, s._vestings[_old].total);
        s._vestings[_old].total = 0;
        s._vestings[_old].released = 0;
        emit OwnerWithdraw(_old);
    }

    function deposit() external beforeTGE {
        s._erc20Token.transferFrom(msg.sender, address(this), s._total - s._erc20Token.balanceOf(address(this)));
    }

    function changeAmount(address _add, uint256 _amount) external onlyOwner{
        s._total -= s._vestings[_add].total;
        s._vestings[_add] = Vesting(_amount, 0);
        s._total += _amount;
        emit Edit(_add, _amount);
    }

    /**
     * Owner withdraw beneficiary (use for incorrect wallet data)
     */
    function ownerWithdraw(address beneficiary) external onlyOwner {
        uint256 remain = remaining(beneficiary);
        require(
            remain > 0,
            "Bizverse: Beneficiary not exist or claim over"
        );
        s._erc20Token.transfer(msg.sender, remain);
        s._total -= remain;
        s._vestings[beneficiary].total = 0;
        s._vestings[beneficiary].released = 0;
        emit OwnerWithdraw(beneficiary);
    }

}
