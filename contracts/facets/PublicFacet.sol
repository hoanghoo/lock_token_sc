//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;

import "@openzeppelin/contracts/interfaces/IERC20Metadata.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "./CoreFacet.sol";

contract PublicFacet is CoreFacet {
    using Address for address;
    using SafeERC20 for IERC20;
    using SafeMath for uint256;

    /**
     * Get token released
     */
    function getRelease(address _ownLock)
        public
        view
        returns (uint256)
    {
        if (block.timestamp < tgeTimestamp()) return 0;
        else return vested(_ownLock) - released(_ownLock);
    }

    function calcReleased(address[] calldata beneficiaries) external {
        s._claimable = 0;
        for (uint i = 0; i < beneficiaries.length; i++) {
            s._claimable += vested(beneficiaries[i]);
        }
    }

    /**
     * User call to claim token
     */
    function release() external afterTGE ownLock(msg.sender) nonReentrant {
        uint256 releasable = getRelease(msg.sender);
        require(releasable != 0, "Bizverse: Nothing to release");
        require(s._erc20Token.transfer(msg.sender, releasable), "BIVE: Transfer token failure");
        s._vestings[msg.sender].released += releasable;
        emit Release(msg.sender, releasable);
    }


    function vested(address user) public view returns (uint256) {
        return _vestingSchedule(s._vestings[user].total, uint64(block.timestamp));
    }

    function _vestingSchedule(uint256 totalAllocation, uint64 timestamp)
        internal
        view
        returns (uint256)
    {
        if(timestamp < s._tgeTimestamp) return 0;
        else return totalAllocation;
    }
}
