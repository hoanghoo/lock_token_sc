//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;

import "@openzeppelin/contracts/interfaces/IERC20Metadata.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "./CoreFacet.sol";

contract EcosystemFacet is CoreFacet {
    using Address for address;
    using SafeERC20 for IERC20;
    using SafeMath for uint256;

    function tgeTimestampt() external view returns(uint256){
        return s._tgeTimestamp;
    }


    /**
     * User call to claim token
     */
    function release(uint256 _amount) external afterTGE ownLock(msg.sender) nonReentrant {
        require(s._erc20Token.transfer(msg.sender, _amount), "BIVE: Transfer token failure");
        emit Release(msg.sender, _amount);
    }

}
