//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;

import "hardhat-deploy/solc_0.8/diamond/UsingDiamondOwner.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";

// DO NOT MODIFIY FIELDS ORDER
// JUST ADD NEW FIELDS

struct Vesting {
    uint256 total;
    uint256 released;
}

struct AppStorage {

    /**
     * Metadata
     */
    uint256 _claimable;
    uint256 _total;

    // Vesting wallet for every user
    mapping(address => Vesting) _vestings;

    // Whitelist
    mapping(address => bool) _whitelist;

    // Define token
    IERC20 _erc20Token;

    // Define time point
    uint64 _tgeTimestamp;
    uint64 _vestingStart;

    // Define time
    // the vesting will end at the timestamp: _vestingStart + (_totalRound * _timePerRound)
    uint _totalRound;
    uint256 _timePerRound;

    // Define Percent
    // i.e 3% = 300, 50% = 5000
    uint32 _percentForTGE;
    uint32 _percentForVesting;

    // Time constant
    uint timePerWeek;
}

library LibAppStorage {
    function diamondStorage() internal pure returns (AppStorage storage s) {
        assembly {
            s.slot := 0
        }
    }
}

contract Modifiers is UsingDiamondOwner {
    AppStorage internal s;

  modifier beforeTGE() {
        require(
            block.timestamp < s._tgeTimestamp,
            "Bizverse: Action must be before TGE"
        );
        _;
    }

    modifier afterTGE() {
        require(
            block.timestamp >= s._tgeTimestamp,
            "Bizverse: Action must be after TGE"
        );
        _;
    }

    modifier ownLock(address user) {
        require(
            s._vestings[user].total > 0,
            "Bizverse: Nothing to release"
        );
        _;
    }
}