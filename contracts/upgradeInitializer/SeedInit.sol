//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;
import "hardhat-deploy/solc_0.8/diamond/libraries/LibDiamond.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/interfaces/IERC20Metadata.sol";
import "../libraries/LibAppStorage.sol";
import "../facets/OwnableFacet.sol";

contract SeedInit is Modifiers, OwnableFacet{
    struct Init {
        IERC20 erc20Token_;
        uint64 tgeTimestamp_;
        uint64 vestingStart_;
        uint totalRound_;
        uint timePerRound_;
        uint16 percentForTGE_;
    }

    function init(Init calldata data) external {
        // Check data
        require(
            data.vestingStart_ >= data.tgeTimestamp_,
            "Bizverse: Now < TGE <= Vesting Start"
        );
        require(data.totalRound_ > 0 && data.timePerRound_ > 0, "Bizverse: Round must be positive");
        require(data.percentForTGE_ < 100000, "Bizverse: Percent value invalid");
        
        s._erc20Token = data.erc20Token_;
        s._claimable = 0;
        s._total = 0;
        s._tgeTimestamp = data.tgeTimestamp_;
        s._vestingStart = data.vestingStart_;
        s._totalRound = data.totalRound_;
        s._timePerRound = data.timePerRound_;
        s._percentForTGE = data.percentForTGE_;
        s._percentForVesting = 100000 - s._percentForTGE;
        s.timePerWeek = 604800; //604800 - 3600
        transferOwnership(msg.sender);
    }
}
