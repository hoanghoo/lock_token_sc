//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;
import "hardhat-deploy/solc_0.8/diamond/libraries/LibDiamond.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/interfaces/IERC20Metadata.sol";
import "../libraries/LibAppStorage.sol";
import "../facets/OwnableFacet.sol";

contract PublicInit is Modifiers, OwnableFacet{
    struct Init {
        IERC20 erc20Token_;
        uint64 tgeTimestamp_;
        uint timePerRound_;
    }

    function init(Init calldata data) external {
        // Check data
        require(
            data.tgeTimestamp_ > block.timestamp,
            "Bizverse: Now < TGE"
        );
        
        s._erc20Token = data.erc20Token_;
        s._claimable = 0;
        s._total = 0;
        s._tgeTimestamp = data.tgeTimestamp_;
        s._vestingStart = data.tgeTimestamp_;
        s._totalRound = 0;
        s._timePerRound = data.timePerRound_;
        s._percentForTGE = 100000;
        s._percentForVesting = 0;
        s.timePerWeek = 604800; //604800 - 3600
        transferOwnership(msg.sender);
    }
}
