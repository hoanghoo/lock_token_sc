import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("marketing-Prod", deployerBeta);
  // const addresses = ["0x219FEd9F73A0e125ab97FC053C9700dc405a445b"];

  // const originAmount = [100000000];

  // const amounts = originAmount.map((val) => {
  //   return eth.utils.parseUnits(val.toString(), 4);
  // });

  // const tx = await contract.batchLock(addresses, amounts);

  // await tx.wait();

  // console.log(tx); // True

  // console.log(await contract.total());

  const tx = await contract.transferOwnership(
    "0x2C7ec33367e3B4903AdE37c824cFe65C86698f9a"
  );
  // await tx.wait();
  console.log(tx.hash);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
