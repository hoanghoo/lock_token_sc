import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("seed-Prod", deployerBeta);
  const addresses = ["0xFDcc2c8743A8cA31dE968F559933dBa59254E876"];

  const originAmount = [500000];

  const amounts = originAmount.map((val) => {
    return eth.utils.parseUnits(val.toString(), 4);
  });

  // const tx = await contract.batchLock(
  //   ["0x89455D8e42199be3a71F1928D90992A1CB6E6e42"],
  //   [eth.utils.parseUnits("1666667", 4)]
  // );
  const tx = await contract.lockAddress(
    "0xba22c859b4df135Fbf128B0D263CbFF29BffB7Ab"
  );
  // const tx = await contract.changeWallet(
  //   "0xF09bDc3CD16e2D76BB097037B9D26f8940aeD524",
  //   "0xba22c859b4df135Fbf128B0D263CbFF29BffB7Ab"
  // );
  // await tx.wait();

  console.log(tx); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
