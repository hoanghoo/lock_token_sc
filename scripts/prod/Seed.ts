import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("seed-Prod", deployerBeta);
  // const addresses = [
  //   "0x105C972C27f55b71A37dEF4C1678B616C068fa09",
  //   "0x117cC8c3DB4e692c9eA4eE9d8a4027b62d8fa981",
  //   "0x26fdF263CBDb0D1b3c288d6d601B3Abc4b9ffB2C",
  //   "0x2c33a1D606A7af8A9034CECEF0CAd6e92711fE27",
  //   "0x5132C9a126ae9b32479488805301807770D409a3",
  //   "0x53d828478c6e50B08B67e838701B42b52C9f8Cb1",
  //   "0x5D50CE4884aD860f0689312a1D7a724399f3E90f",
  //   "0x5F35C650998A5C43a69073BE8fB26A43b15D1e99",
  //   "0x88a88C568984ec40b80166E9CDcd6fd0e42CD866",
  //   "0x88b6f5EC10885596BdeB33932E7F96e43C0D41Aa",
  //   "0x94eB5A698ED65F3b62db22E4006D2775d4f11493",
  //   "0xa393FE08cA6AfC4d55d0E97Cc52Ac3049a49c7dC",
  //   "0xA53CfeFbE0bD7C16e3f0232388C37d9232E9a466",
  //   "0xae9e787684Ad954297664AeeF83eD1B84ECe174d",
  //   "0xc36b0bde3e10627eec892de6e860308b9aeeb259",
  //   "0xC95B1Dce10493f98FCb5dd21462F8d7BA425BF98",
  //   "0xca47Ca762494367E3BF8cdC661f43DC4FA868d41",
  //   "0xCF9BbDCa4EEC1076274De2167E30FE0655420dEE",
  //   "0xd0FC5beDAd21A28bDF2984bbD22Da4533ed92993",
  //   "0xD24cD68D6Ae970BbEfE03964387757918a0efB31",
  //   "0xDF8853C4467Bce27d6C05edf1cD6EC4C38e7F6E0",
  //   "0xE4F7301641BA610cA52777277Ad4cAC9A306F341",
  //   "0xeBc0832A15AeC8497d63ad30782237B6cb3dC544",
  //   "0xF09bDc3CD16e2D76BB097037B9D26f8940aeD524",
  // ];

  // const originAmount = [
  //   392139, 16666667, 25000, 200000, 9951568, 625786, 3413836, 304403, 266667,
  //   40000, 533963, 1125786, 20000, 133333, 500000, 5000000, 4195598, 233334,
  //   25000, 16667, 3839623, 350000, 433963, 40000,
  // ];

  // const amounts = originAmount.map((val) => {
  //   return eth.utils.parseUnits(val.toString(), 4);
  // });

  // // const tx = await contract.batchLock(addresses, amounts);
  // const tx = await contract.lockAddress(
  //   "0x88a88C568984ec40b80166E9CDcd6fd0e42CD866"
  // );
  // // await tx.wait();

  // console.log(tx); // True

  const tx = await contract.transferOwnership(
    "0x2C7ec33367e3B4903AdE37c824cFe65C86698f9a"
  );
  // await tx.wait();
  console.log(tx.hash);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
