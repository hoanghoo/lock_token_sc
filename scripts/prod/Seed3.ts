import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("seed-Prod", deployerBeta);
  const addresses = ["0xFDcc2c8743A8cA31dE968F559933dBa59254E876"];

  const originAmount = [500000];

  const amounts = originAmount.map((val) => {
    return eth.utils.parseUnits(val.toString(), 4);
  });

  // const tx = await contract.batchLock(
  //   ["0xeD29aAC5e428d90Ee9E62036fb98C3191f47db60"],
  //   [eth.utils.parseUnits("62500", 4)]
  // );
  // const tx = await contract.lockAddress(
  //   "0x5132C9a126ae9b32479488805301807770D409a3"
  // );
  // const tx = await contract.changeWallet(
  //   "0xc36b0bde3e10627eec892de6e860308b9aeeb259",
  //   "0xcC65C1405CC7A80dbDc278ff5E27Df46c4fc7d66"
  // );
  // await tx.wait();

  const tx = await contract.changeAmount(
    "0x2c33a1D606A7af8A9034CECEF0CAd6e92711fE27",
    eth.utils.parseUnits("116666", 4)
  );

  console.log(tx); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
