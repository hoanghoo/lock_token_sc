import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("community-Prod", deployerBeta);
  const tx = await contract.transferOwnership(
    "0x2C7ec33367e3B4903AdE37c824cFe65C86698f9a"
  );
  // await tx.wait();
  console.log(tx.hash);
  // const addresses = ["0x1B9fb65c2ea8d90B104555aB97B4E6fd6E71b26b"];

  // const originAmount = [30000000];

  // const amounts = originAmount.map((val) => {
  //   return eth.utils.parseUnits(val.toString(), 4);
  // });

  // const tx = await contract.batchLock(addresses, amounts);

  // await tx.wait();

  // console.log(tx); // True

  // console.log(await contract.total());
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
