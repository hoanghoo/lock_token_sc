import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("ecosystem-Prod", deployerBeta);

  const tx = await contract.transferOwnership(
    "0x2C7ec33367e3B4903AdE37c824cFe65C86698f9a"
  );
  // await tx.wait();
  console.log(tx.hash);
  // const addresses = ["0x40340F6724CDb4a928fcE2Ad8359Ea2B68A28D47"];

  // const originAmount = [270000000];

  // const amounts = originAmount.map((val) => {
  //   return eth.utils.parseUnits(val.toString(), 4);
  // });

  // const tx = await contract.batchLock(addresses, amounts);

  // await tx.wait();

  // console.log(tx); // True

  // console.log(await contract.total());
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
