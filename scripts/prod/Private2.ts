import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("private-Prod", deployerBeta);
  const addresses = ["0xFDcc2c8743A8cA31dE968F559933dBa59254E876"];

  const originAmount = [500000];

  const amounts = originAmount.map((val) => {
    return eth.utils.parseUnits(val.toString(), 4);
  });

  // const tx = await contract.batchLock(
  //   ["0x17E04a1abb55b392f29D058343faBCAa23503Ec6"],
  //   [eth.utils.parseUnits("100000", 4)]
  // );
  // const tx = await contract.lockAddress(
  //   "0x17E04a1abb55b392f29D058343faBCAa23503Ec6"
  // );

  // const tx = await contract.lockAddress(
  //   "0xF09bDc3CD16e2D76BB097037B9D26f8940aeD524"
  // );

  // const tx = await contract.changeWallet(
  //   "0xF09bDc3CD16e2D76BB097037B9D26f8940aeD524",
  //   "0xba22c859b4df135Fbf128B0D263CbFF29BffB7Ab"
  // );
  // await tx.wait();

  // await tx.wait();

  console.log(
    await contract.lockAddress("0x17E04a1abb55b392f29D058343faBCAa23503Ec6")
  ); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
