import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("publicV2-Prod", deployerBeta);
  const addresses = ["0x980be8B5ff783855e91DDb6CDd4BD310fF81bFC2"];

  const originAmount = [7500];

  const amounts = originAmount.map((val) => {
    return eth.utils.parseUnits(val.toString(), 4);
  });

  // const tx = await contract.changeAmount(addresses[0], 0);
  // const tx = await contract.transfer();
  console.log(await contract.vestingStartTimestamp());
  console.log(await contract.percentForTGE());
  // await tx.wait();

  // console.log(tx); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
