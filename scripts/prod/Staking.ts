import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("staking-Prod", deployerBeta);
  // const addresses = ["0x6C0B42cb486Ce0B1F7787bd741B7F0542589A96E"];

  // const originAmount = [100000000];

  // const amounts = originAmount.map((val) => {
  //   return eth.utils.parseUnits(val.toString(), 4);
  // });

  // const tx = await contract.batchLock(addresses, amounts);

  // await tx.wait();
  // console.log(await contract.total());
  // console.log(tx); // True

  const tx = await contract.transferOwnership(
    "0x2C7ec33367e3B4903AdE37c824cFe65C86698f9a"
  );
  await tx.wait();
  console.log(tx.hash);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
