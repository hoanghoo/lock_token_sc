import { ethers, getNamedAccounts } from "hardhat";
import * as eth from "ethers";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("private-Prod", deployerBeta);
  // const addresses = [
  //   "0x008bfa042E0C39566706E78C34EA40012AcBE798",
  //   "0x0B0D38625629173176327EE715642927244d154E",
  //   "0x105C972C27f55b71A37dEF4C1678B616C068fa09",
  //   "0x19e60908623F3c14f4F4a50DdEB78De558cb0450",
  //   "0x1B285DaDcB30aD3541B96cc3d4310AbcEE975252",
  //   "0x1d34627FB21F1c97baA48E825eC4d708276E9509",
  //   "0x231f3B20f55786Ff12b275756eD13569bcd2c130",
  //   "0x26fdF263CBDb0D1b3c288d6d601B3Abc4b9ffB2C",
  //   "0x27e73621Bf749d0A30D42d2fc418437a4c7B691C",
  //   "0x282e451FcE3e4C4176d5bb91cf6c9A27c37332D1",
  //   "0x29CfEe1d3cA341066592df27DF87EF0076af5C37",
  //   "0x2A25cCDBacE30d35ecF21e830039811e16E5D81b",
  //   "0x2eE22F4A033B436Cd9b185DFD56beCF64f2E3Fc1",
  //   "0x31891f2e611352002D4C0d801632c621ad3127f9",
  //   "0x33CC05D5A5E948e075368413c06dFCEb14b846Fd",
  //   "0x33E6Ad094E348D9EE6Cf703d19cecfc75c44b931",
  //   "0x3bE7d5491c7241637671cb9E7667966b0dF8C847",

  //   "0x85b797222Edc0A577eEFEBe7dBf2Dc266964459C",
  //   "0x88a7DA3db8Ec77d526AE779AE70F6A124A525f90",
  //   "0x88a88C568984ec40b80166E9CDcd6fd0e42CD866",
  //   "0x88b6f5EC10885596BdeB33932E7F96e43C0D41Aa",
  // ];

  // const originAmount = [
  //   5000, 616667, 383333, 2777778, 16667, 466667, 166667, 85417, 3472, 1042,
  //   8333333, 25000, 700, 1500000, 16667, 5000, 1500000, 1736, 6667, 83333, 5500,
  //   400000, 333333, 11433, 750000, 333333, 7500, 158333, 100000, 50000, 925926,
  //   1000, 8333, 16667, 16666667, 333333, 1783333, 25000, 333333, 33333, 375000,
  //   433333, 125000, 18333, 3045140, 700, 71667, 50000, 150000, 166667, 66667,
  //   133333, 131667, 333333, 400000, 21667, 366667, 700, 500000, 3667, 83333,
  //   3667, 16667, 16667, 366666, 26667, 2250000, 333333, 212767, 52083, 25000,
  //   33333, 1000, 104167,
  // ];

  // const amounts = originAmount.map((val) => {
  //   return eth.utils.parseUnits(val.toString(), 4);
  // });

  // const tx = await contract.batchLock(addresses, amounts);
  // // const tx = await contract.lockAddress(
  // //   "0x88a88C568984ec40b80166E9CDcd6fd0e42CD866"
  // // );
  // await tx.wait();

  // console.log(tx); // True

  const tx = await contract.transferOwnership(
    "0x2C7ec33367e3B4903AdE37c824cFe65C86698f9a"
  );
  // await tx.wait();
  console.log(tx.hash);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
