/* eslint-disable node/no-unpublished-import */
import { expect } from "chai";
import { ethers, deployments, getNamedAccounts } from "hardhat";
import { PublicFacet, BIVE } from "../typechain";
import { time, takeSnapshot } from "@nomicfoundation/hardhat-network-helpers";
import { BigNumber, constants } from "ethers";
import { parseUnits } from "ethers/lib/utils";

const now = Math.ceil(new Date().getTime() / 1000);
const hour = 3600;
const day = hour * 24;
const week = day * 7;
const month = day * 30;

const setup = deployments.createFixture(async () => {
  const { deployerTest, test1, test2, test3, test4 } = await getNamedAccounts();
  await deployments.fixture("public-Dev");
  const bive = await ethers.getContract("bive-Dev", deployerTest);
  const owner = await ethers.getContract("public-Dev", deployerTest);
  const buyer1 = await ethers.getContract("public-Dev", test1);
  const buyer2 = await ethers.getContract("public-Dev", test2);
  const buyer3 = await ethers.getContract("public-Dev", test3);
  const noBuyer = await ethers.getContract("public-Dev", test4);
  return {
    iPublic: <PublicFacet>owner,
    iBive: <BIVE>bive,
    ownerAddress: deployerTest,
    buyer1Address: test1,
    buyer2Address: test2,
    buyer3Address: test3,
    noBuyerAddress: test4,
    bive,
    owner,
    buyer1,
    buyer2,
    buyer3,
    noBuyer,
    data: {
      beneficiaries: [test1, test2, test3],
      amounts: [
        parseUnits("4666667", 4),
        parseUnits("333333", 4),
        parseUnits("5000000", 4),
      ],
    },
  };
});

describe("Owner Permission", function () {
  it("init data correct", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data } = await setup();
    expect(await owner.token()).to.be.equal(bive.address);
    const cliffTime = (await owner.vestingStartTimestamp()).sub(
      await owner.tgeTimestamp()
    );
    expect(cliffTime.toNumber(10)).to.be.equal(0);
    expect(await owner.percentForTGE()).to.be.equal(50000);
    expect(await owner.percentForVesting()).to.be.equal(50000);
    expect(await owner.totalRound()).to.be.equal(5);
    expect(await owner.timePerRound()).to.be.equal(month);
    await owner.calcReleased(data.beneficiaries);
    expect(await owner.total()).to.be.equal(0);
    expect(await owner.currentReleasable()).to.be.equal(0);
    snapshot.restore();
  });

  it("batchLock succeed before TGE", async function () {
    const snapshot = await takeSnapshot();
    const { owner, buyer1, data, iPublic } = await setup();
    await expect(owner.batchLock(data.beneficiaries, data.amounts))
      .to.emit(iPublic, "BatchLock")
      .withArgs(data.beneficiaries, data.amounts);
    await expect(buyer1.batchLock(data.beneficiaries, data.amounts)).to.be
      .reverted;
    await owner.calcReleased(data.beneficiaries);
    expect(await owner.total()).to.be.equal(parseUnits("10000000", 4));
    expect(await owner.currentReleasable()).to.be.equal(0);
    await time.increase(2 * hour);
    await expect(
      owner.batchLock(data.beneficiaries, data.amounts)
    ).to.be.revertedWith("Bizverse: Action must be before TGE");
    snapshot.restore();
  });

  it("deposit succeed before TGE", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, buyer1, data, iPublic } = await setup();
    await bive.approve(owner.address, constants.MaxUint256);
    await expect(owner.deposit()).to.be.not.reverted;
    expect(await owner.total()).to.be.equal(
      await bive.balanceOf(owner.address)
    );
    await time.increase(2 * hour);
    await expect(owner.deposit()).to.be.revertedWith(
      "Bizverse: Action must be before TGE"
    );
    snapshot.restore();
  });

  it("withdraw succeed", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, iPublic, noBuyerAddress } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    await expect(owner.ownerWithdraw(data.beneficiaries[2]))
      .to.emit(iPublic, "OwnerWithdraw")
      .withArgs(data.beneficiaries[2]);
    expect(await owner.total()).to.be.equal(parseUnits("5000000", 4));
    expect((await owner.lockAddress(data.beneficiaries[2])).total).to.be.equal(
      0
    );
    expect(
      (await owner.lockAddress(data.beneficiaries[2])).released
    ).to.be.equal(0);

    expect(await owner.total()).to.be.equal(
      await bive.balanceOf(owner.address)
    );

    await expect(owner.ownerWithdraw(noBuyerAddress)).to.be.revertedWith(
      "Bizverse: Beneficiary not exist or claim over"
    );

    snapshot.restore();
  });
});

describe("Buyer Permission", function () {
  it("data correct", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, noBuyerAddress } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    expect((await owner.lockAddress(data.beneficiaries[0])).total).to.be.equal(
      data.amounts[0]
    );
    expect(
      (await owner.lockAddress(data.beneficiaries[0])).released
    ).to.be.equal(0);
    expect((await owner.lockAddress(noBuyerAddress)).total).to.be.equal(0);
    expect((await owner.lockAddress(noBuyerAddress)).released).to.be.equal(0);
    snapshot.restore();
  });

  it("claim before tge revert", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1 } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    await expect(buyer1.release()).to.be.revertedWith(
      "Bizverse: Action must be after TGE"
    );
    snapshot.restore();
  });

  it("token transfered  updated after claim", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1, buyer1Address, iPublic } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    await time.increase(2 * hour);

    await expect(buyer1.release())
      .to.emit(iPublic, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0].mul(BigNumber.from("50")).div(BigNumber.from("100"))
      );

    // Check data
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      data.amounts[0].mul(BigNumber.from("50")).div(BigNumber.from("100"))
    );
    await snapshot.restore();
  });

  it("claim in vesting time - phrase 1", async function () {
    /**
     * Scenario
     * Case 1: Claim in TGE -> claim in vesting 2 times
     * Case 2: Not claim in TGE -> claim in vesting 3 times (buyer2)
     */
    const snapshot = await takeSnapshot();
    const {
      iPublic,
      bive,
      owner,
      data,
      buyer1,
      buyer1Address,
      buyer2,
      buyer2Address,
    } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE time
    await time.increase(2 * hour);

    await expect(buyer1.release())
      .to.emit(iPublic, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0].mul(BigNumber.from("50")).div(BigNumber.from("100"))
      );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0].mul(BigNumber.from("50")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      data.amounts[0].mul(BigNumber.from("50")).div(BigNumber.from("100"))
    );

    // Increse to claim time 1 (1 months) - buyer 1,2
    await time.increase(month);
    await expect(buyer1.release())
      .to.emit(iPublic, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0]
          .mul(BigNumber.from("50"))
          .mul(BigNumber.from("1"))
          .div(BigNumber.from("100"))
          .div(BigNumber.from("5"))
      );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );

    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
        .mul(BigNumber.from("50"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[0]
            .mul(BigNumber.from("50"))
            .mul(BigNumber.from("1"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("5"))
        )
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );

    await expect(buyer2.release())
      .to.emit(iPublic, "Release")
      .withArgs(
        data.beneficiaries[1],
        data.amounts[1]
          .mul(BigNumber.from("50"))
          .div(BigNumber.from("100"))
          .add(
            data.amounts[1]
              .mul(BigNumber.from("50"))
              .mul(BigNumber.from("1"))
              .div(BigNumber.from("100"))
              .div(BigNumber.from("5"))
          )
      );
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1]
        .mul(BigNumber.from("50"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[1]
            .mul(BigNumber.from("50"))
            .mul(BigNumber.from("1"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("5"))
        )
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );

    // Increse to claim time 2 (2 months) - buyer 2
    await time.increase(month);
    await expect(buyer2.release())
      .to.emit(iPublic, "Release")
      .withArgs(
        data.beneficiaries[1],
        data.amounts[1]
          .mul(BigNumber.from("50"))
          .mul(BigNumber.from("1"))
          .div(BigNumber.from("100"))
          .div(BigNumber.from("5"))
      );

    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1]
        .mul(BigNumber.from("50"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[1]
            .mul(BigNumber.from("50"))
            .mul(BigNumber.from("2"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("5"))
        )
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );

    // Increse to claim time 3 (4months) - buyer 1,2
    await time.increase(2 * month);
    await expect(buyer1.release())
      .to.emit(iPublic, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0]
          .mul(BigNumber.from("50"))
          .mul(BigNumber.from("3"))
          .div(BigNumber.from("100"))
          .div(BigNumber.from("5"))
      );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
        .mul(BigNumber.from("50"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[0]
            .mul(BigNumber.from("50"))
            .mul(BigNumber.from("4"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("5"))
        )
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );

    await expect(buyer2.release())
      .to.emit(iPublic, "Release")
      .withArgs(
        data.beneficiaries[1],
        data.amounts[1]
          .mul(BigNumber.from("50"))
          .mul(BigNumber.from("2"))
          .div(BigNumber.from("100"))
          .div(BigNumber.from("5"))
      );
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1]
        .mul(BigNumber.from("50"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[1]
            .mul(BigNumber.from("50"))
            .mul(BigNumber.from("4"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("5"))
        )
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );
    snapshot.restore();
  });

  it("claim in vesting time - phrase 2", async function () {
    /**
     * Scenario
     * Case 1: Claim in TGE -> claim in all round - buyer1
     * Case 2: Not claim in TGE -> claim in all round - buyer2
     */
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1, buyer1Address, buyer2, buyer2Address } =
      await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE time
    await time.increase(2 * hour);
    await buyer1.release();

    for (let i = 1; i <= 5; i++) {
      await time.increase(month);
      // await expect(buyer1.release())
      //   .to.emit(iPublic, "Release")
      //   .withArgs(
      //     data.beneficiaries[0],
      //     data.amounts[0]
      //       .mul(BigNumber.from("50"))
      //       .mul(BigNumber.from("1"))
      //       .div(BigNumber.from("100"))
      //       .div(BigNumber.from("5"))
      //   );
      await buyer1.release();
      expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
        data.amounts[0]
      );
      expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
        data.amounts[0]
          .mul(BigNumber.from("50"))
          .div(BigNumber.from("100"))
          .add(
            data.amounts[0]
              .mul(BigNumber.from("50"))
              .mul(BigNumber.from(i.toString()))
              .div(BigNumber.from("100"))
              .div(BigNumber.from("5"))
          )
      );
      expect(await bive.balanceOf(buyer1Address)).to.be.equal(
        (await owner.lockAddress(buyer1Address)).released
      );

      await buyer2.release();

      expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
        data.amounts[1]
      );
      expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
        data.amounts[1]
          .mul(BigNumber.from("50"))
          .div(BigNumber.from("100"))
          .add(
            data.amounts[1]
              .mul(BigNumber.from("50"))
              .mul(BigNumber.from(i.toString()))
              .div(BigNumber.from("100"))
              .div(BigNumber.from("5"))
          )
      );
      expect(await bive.balanceOf(buyer2Address)).to.be.equal(
        (await owner.lockAddress(buyer2Address)).released
      );
    }

    await expect(buyer1.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );

    await expect(buyer2.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );

    snapshot.restore();
  });

  it("claim in vesting time - phrase 3", async function () {
    /**
     * Scenario
     * Case 1: Claim in TGE -> claim in vesting -> claim after vesting (buyer1)
     * Case 2: Claim in TGE -> claim after vesting (buyer2)
     * Case 3: claim in vesting -> claim after vesting (buyer3)
     */
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1, buyer1Address, buyer2, buyer2Address } =
      await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE time
    await time.increase(2 * hour + week);
    await buyer1.release();
    await buyer2.release();
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0].mul(BigNumber.from("50")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1].mul(BigNumber.from("50")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );

    // Move to vesting time
    await time.increase(month);
    await buyer1.release();
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
        .mul(BigNumber.from("50"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[0]
            .mul(BigNumber.from("50"))
            .mul(BigNumber.from("1"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("5"))
        )
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );

    // Move to after vesting time
    await time.increase(5 * month);
    await buyer1.release();
    await buyer2.release();
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1]
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );

    snapshot.restore();
  });

  it("claim in vesting time - phrase 4", async function () {
    /**
     * Scenario
     * Case 1: Claim after vesting (buyer1)
     */
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1, buyer1Address } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE time
    await time.increase(2 * hour);

    // Move to after vesting time
    await time.increase(6 * month);
    await buyer1.release();

    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    await expect(buyer1.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );
    snapshot.restore();
  });

  it("token release after round correct", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE
    await time.increase(2 * hour + week);

    // Check releasable
    await owner.calcReleased(data.beneficiaries);
    expect(await owner.currentReleasable()).equal(
      parseUnits("10000000", 4)
        .mul(BigNumber.from("50"))
        .div(BigNumber.from("100"))
    );

    // Move to cliff time

    for (let i = 1; i <= 5; i++) {
      await time.increase(month);
      await owner.calcReleased(data.beneficiaries);
      console.log(`${i} ==> ${(await owner.currentReleasable()).toNumber(10)}`);
      expect(await owner.currentReleasable()).equal(
        parseUnits("10000000", 4)
          .mul(BigNumber.from("50"))
          .div(BigNumber.from("100"))
          .add(
            parseUnits("10000000", 4)
              .mul(BigNumber.from("50"))
              .mul(BigNumber.from(i.toString()))
              .div(BigNumber.from("100"))
              .div(BigNumber.from("5"))
          )
      );
    }

    await snapshot.restore();
  });
});
