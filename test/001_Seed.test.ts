/* eslint-disable node/no-unpublished-import */
import { expect } from "chai";
import { ethers, deployments, getNamedAccounts } from "hardhat";
import { SeedFacet, BIVE } from "../typechain";
import { time, takeSnapshot } from "@nomicfoundation/hardhat-network-helpers";
import { BigNumber, constants } from "ethers";
import { parseUnits } from "ethers/lib/utils";

const now = Math.ceil(new Date().getTime() / 1000);
const hour = 3600;
const day = hour * 24;
const week = day * 7;
const month = day * 30;

const initConfig = {
  tgeTimestamp_: now + hour,
  vestingStart_: now + hour + 6 * month,
  totalRound_: 24,
  timePerRound_: month,
  percentForTGE_: 5000,
};

const setup = deployments.createFixture(async () => {
  const { deployerTest, test1, test2, test3, test4 } = await getNamedAccounts();
  await deployments.fixture("seed-Dev");
  const bive = await ethers.getContract("bive-Dev", deployerTest);
  const owner = await ethers.getContract("seed-Dev", deployerTest);
  const buyer1 = await ethers.getContract("seed-Dev", test1);
  const buyer2 = await ethers.getContract("seed-Dev", test2);
  const buyer3 = await ethers.getContract("seed-Dev", test3);
  const noBuyer = await ethers.getContract("seed-Dev", test4);
  return {
    iSeed: <SeedFacet>owner,
    iBive: <BIVE>bive,
    ownerAddress: deployerTest,
    buyer1Address: test1,
    buyer2Address: test2,
    buyer3Address: test3,
    noBuyerAddress: test4,
    bive,
    owner,
    buyer1,
    buyer2,
    buyer3,
    noBuyer,
    data: {
      beneficiaries: [test1, test2, test3],
      amounts: [
        parseUnits("4666667", 4),
        parseUnits("333333", 4),
        parseUnits("45000000", 4),
      ],
    },
  };
});

describe("Owner Permission", function () {
  it("init data correct", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data } = await setup();
    expect(await owner.token()).to.be.equal(bive.address);
    const cliffTime = (await owner.vestingStartTimestamp()).sub(
      await owner.tgeTimestamp()
    );
    expect(cliffTime.toNumber(10)).to.be.equal(6 * month);
    expect(await owner.percentForTGE()).to.be.equal(5000);
    expect(await owner.percentForVesting()).to.be.equal(95000);
    expect(await owner.totalRound()).to.be.equal(24);
    expect(await owner.timePerRound()).to.be.equal(month);
    await owner.calcReleased(data.beneficiaries);
    expect(await owner.total()).to.be.equal(0);
    expect(await owner.currentReleasable()).to.be.equal(0);
    snapshot.restore();
  });

  it("batchLock succeed before TGE", async function () {
    const snapshot = await takeSnapshot();
    const { owner, buyer1, data, iSeed } = await setup();
    await expect(owner.batchLock(data.beneficiaries, data.amounts))
      .to.emit(iSeed, "BatchLock")
      .withArgs(data.beneficiaries, data.amounts);
    await expect(buyer1.batchLock(data.beneficiaries, data.amounts)).to.be
      .reverted;
    await owner.calcReleased(data.beneficiaries);
    expect(await owner.total()).to.be.equal(parseUnits("50000000", 4));
    expect(await owner.currentReleasable()).to.be.equal(0);
    await time.increase(2 * hour);
    await expect(
      owner.batchLock(data.beneficiaries, data.amounts)
    ).to.be.revertedWith("Bizverse: Action must be before TGE");
    snapshot.restore();
  });

  it("deposit succeed before TGE", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, buyer1, data, iSeed } = await setup();
    await bive.approve(owner.address, constants.MaxUint256);
    await expect(owner.deposit()).to.be.not.reverted;
    expect(await owner.total()).to.be.equal(
      await bive.balanceOf(owner.address)
    );
    await time.increase(2 * hour);
    await expect(owner.deposit()).to.be.revertedWith(
      "Bizverse: Action must be before TGE"
    );
    snapshot.restore();
  });

  it("withdraw succeed", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, iSeed, noBuyerAddress } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    await expect(owner.ownerWithdraw(data.beneficiaries[2]))
      .to.emit(iSeed, "OwnerWithdraw")
      .withArgs(data.beneficiaries[2]);
    expect(await owner.total()).to.be.equal(parseUnits("5000000", 4));
    expect((await owner.lockAddress(data.beneficiaries[2])).total).to.be.equal(
      0
    );
    expect(
      (await owner.lockAddress(data.beneficiaries[2])).released
    ).to.be.equal(0);

    expect(await owner.total()).to.be.equal(
      await bive.balanceOf(owner.address)
    );

    await expect(owner.ownerWithdraw(noBuyerAddress)).to.be.revertedWith(
      "Bizverse: Beneficiary not exist or claim over"
    );

    snapshot.restore();
  });
});

describe("Buyer Permission", function () {
  it("data correct", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, noBuyerAddress } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    expect((await owner.lockAddress(data.beneficiaries[0])).total).to.be.equal(
      data.amounts[0]
    );
    expect(
      (await owner.lockAddress(data.beneficiaries[0])).released
    ).to.be.equal(0);
    expect((await owner.lockAddress(noBuyerAddress)).total).to.be.equal(0);
    expect((await owner.lockAddress(noBuyerAddress)).released).to.be.equal(0);
    snapshot.restore();
  });

  it("claim before tge revert", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1 } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    await expect(buyer1.release()).to.be.revertedWith(
      "Bizverse: Action must be after TGE"
    );
    snapshot.restore();
  });

  it("claim tge < x < tge + 1 week", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1 } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    await time.increase(2 * hour);
    await expect(buyer1.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );
    snapshot.restore();
  });

  it("token transfered  updated after claim", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1, buyer1Address, iSeed } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    await time.increase(2 * hour + week);

    await expect(buyer1.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
      );

    // Check data
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    await snapshot.restore();
  });

  it("claim in cliff time - phrase 1", async function () {
    const snapshot = await takeSnapshot();
    const {
      bive,
      owner,
      data,
      buyer1,
      buyer1Address,
      buyer2,
      buyer2Address,
      buyer3,
      buyer3Address,
      noBuyer,
      iSeed,
    } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    await time.increase(2 * hour + week);

    // Check releasable
    await owner.calcReleased(data.beneficiaries);
    expect(await owner.currentReleasable()).equal(
      parseUnits("50000000", 4)
        .mul(BigNumber.from("5"))
        .div(BigNumber.from("100"))
    );

    /**
     * Claim in TGE
     * Case 1: Claim after TGE a week -> 5%
     * Case 2: Claim dupticate -> revert
     */

    // Case 1: Buyer1 claim
    await expect(buyer1.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
      );
    // Case 2
    await expect(buyer1.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );
    // Check data
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await owner.total()).to.be.equal(parseUnits("50000000", 4));

    // Case 1: Buyer2 claim
    await expect(buyer2.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[1],
        data.amounts[1].mul(BigNumber.from("5")).div(BigNumber.from("100"))
      );
    // Check data
    await expect(buyer2.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      data.amounts[1].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await owner.total()).to.be.equal(parseUnits("50000000", 4));

    // Case 1: Buyer3 claim
    await expect(buyer3.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[2],
        data.amounts[2].mul(BigNumber.from("5")).div(BigNumber.from("100"))
      );
    await expect(buyer3.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );
    // Check data
    expect((await owner.lockAddress(buyer3Address)).total).to.be.equal(
      data.amounts[2]
    );
    expect((await owner.lockAddress(buyer3Address)).released).to.be.equal(
      data.amounts[2].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer3Address)).to.be.equal(
      data.amounts[2].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await owner.total()).to.be.equal(parseUnits("50000000", 4));

    // No buy - no claim anything
    await expect(noBuyer.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );
    await snapshot.restore();
  });

  it("claim in cliff time - phrase 2", async function () {
    const snapshot = await takeSnapshot();
    const {
      iSeed,
      bive,
      owner,
      data,
      buyer1,
      buyer1Address,
      buyer2,
      buyer2Address,
    } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    // Move to TGE time
    await time.increase(2 * hour + week);
    await expect(buyer1.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
      );
    // Check
    // Check data
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );

    // Move to cliff time
    await time.increase(5 * month);
    await expect(buyer1.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );
    await expect(buyer2.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[1],
        data.amounts[1].mul(BigNumber.from("5")).div(BigNumber.from("100"))
      );
    // Check data
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      data.amounts[1].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    snapshot.restore();
  });

  it("claim in vesting time - phrase 1", async function () {
    /**
     * Scenario
     * Case 1: Claim in TGE -> claim in vesting 2 times
     * Case 2: Not claim in TGE -> claim in vesting 3 times (buyer2)
     */
    const snapshot = await takeSnapshot();
    const {
      iSeed,
      bive,
      owner,
      data,
      buyer1,
      buyer1Address,
      buyer2,
      buyer2Address,
    } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE time
    await time.increase(2 * hour + week);

    await expect(buyer1.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
      );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );

    // Increse to claim time 1 (6 months) - buyer 1,2
    await time.increase(7 * month);
    await expect(buyer1.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0]
          .mul(BigNumber.from("95"))
          .mul(BigNumber.from("1"))
          .div(BigNumber.from("100"))
          .div(BigNumber.from("24"))
      );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );

    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
        .mul(BigNumber.from("5"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[0]
            .mul(BigNumber.from("95"))
            .mul(BigNumber.from("1"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("24"))
        )
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );

    await expect(buyer2.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[1],
        data.amounts[1]
          .mul(BigNumber.from("5"))
          .div(BigNumber.from("100"))
          .add(
            data.amounts[1]
              .mul(BigNumber.from("95"))
              .mul(BigNumber.from("1"))
              .div(BigNumber.from("100"))
              .div(BigNumber.from("24"))
          )
      );
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1]
        .mul(BigNumber.from("5"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[1]
            .mul(BigNumber.from("95"))
            .mul(BigNumber.from("1"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("24"))
        )
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );

    // Increse to claim time 2 (12 months) - buyer 2
    await time.increase(6 * month);
    await expect(buyer2.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[1],
        data.amounts[1]
          .mul(BigNumber.from("95"))
          .mul(BigNumber.from("6"))
          .div(BigNumber.from("100"))
          .div(BigNumber.from("24"))
      );

    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1]
        .mul(BigNumber.from("5"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[1]
            .mul(BigNumber.from("95"))
            .mul(BigNumber.from("7"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("24"))
        )
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );

    // Increse to claim time 3 - buyer 1,2
    await time.increase(6 * month);
    await expect(buyer1.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0]
          .mul(BigNumber.from("95"))
          .mul(BigNumber.from("12"))
          .div(BigNumber.from("100"))
          .div(BigNumber.from("24"))
      );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
        .mul(BigNumber.from("5"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[0]
            .mul(BigNumber.from("95"))
            .mul(BigNumber.from("13"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("24"))
        )
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );

    await expect(buyer2.release())
      .to.emit(iSeed, "Release")
      .withArgs(
        data.beneficiaries[1],
        data.amounts[1]
          .mul(BigNumber.from("95"))
          .mul(BigNumber.from("6"))
          .div(BigNumber.from("100"))
          .div(BigNumber.from("24"))
      );
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1]
        .mul(BigNumber.from("5"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[1]
            .mul(BigNumber.from("95"))
            .mul(BigNumber.from("13"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("24"))
        )
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );
    snapshot.restore();
  });

  it("claim in vesting time - phrase 2", async function () {
    /**
     * Scenario
     * Case 1: Claim in TGE -> claim in all round - buyer1
     * Case 2: Not claim in TGE -> claim in all round - buyer2
     */
    const snapshot = await takeSnapshot();
    const {
      iSeed,
      bive,
      owner,
      data,
      buyer1,
      buyer1Address,
      buyer2,
      buyer2Address,
    } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE time
    await time.increase(2 * hour + week);

    await buyer1.release();
    await time.increase(6 * month);

    for (let i = 1; i <= 24; i++) {
      await time.increase(month);
      // await expect(buyer1.release())
      //   .to.emit(iSeed, "Release")
      //   .withArgs(
      //     data.beneficiaries[0],
      //     data.amounts[0]
      //       .mul(BigNumber.from("95"))
      //       .mul(BigNumber.from("1"))
      //       .div(BigNumber.from("100"))
      //       .div(BigNumber.from("24"))
      //   );
      await buyer1.release();
      expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
        data.amounts[0]
      );
      expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
        data.amounts[0]
          .mul(BigNumber.from("5"))
          .div(BigNumber.from("100"))
          .add(
            data.amounts[0]
              .mul(BigNumber.from("95"))
              .mul(BigNumber.from(i.toString()))
              .div(BigNumber.from("100"))
              .div(BigNumber.from("24"))
          )
      );
      expect(await bive.balanceOf(buyer1Address)).to.be.equal(
        (await owner.lockAddress(buyer1Address)).released
      );

      await buyer2.release();

      expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
        data.amounts[1]
      );
      expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
        data.amounts[1]
          .mul(BigNumber.from("5"))
          .div(BigNumber.from("100"))
          .add(
            data.amounts[1]
              .mul(BigNumber.from("95"))
              .mul(BigNumber.from(i.toString()))
              .div(BigNumber.from("100"))
              .div(BigNumber.from("24"))
          )
      );
      expect(await bive.balanceOf(buyer2Address)).to.be.equal(
        (await owner.lockAddress(buyer2Address)).released
      );
    }

    await expect(buyer1.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );

    await expect(buyer2.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );

    snapshot.restore();
  });

  it("claim in vesting time - phrase 3", async function () {
    /**
     * Scenario
     * Case 1: Claim in TGE -> claim in vesting -> claim after vesting (buyer1)
     * Case 2: Claim in TGE -> claim after vesting (buyer2)
     * Case 3: claim in vesting -> claim after vesting (buyer3)
     */
    const snapshot = await takeSnapshot();
    const {
      iSeed,
      bive,
      owner,
      data,
      buyer1,
      buyer1Address,
      buyer2,
      buyer2Address,
      buyer3,
      buyer3Address,
    } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE time
    await time.increase(2 * hour + week);
    await buyer1.release();
    await buyer2.release();
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1].mul(BigNumber.from("5")).div(BigNumber.from("100"))
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );

    // Move to vesting time
    await time.increase(7 * month);
    await buyer1.release();
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
        .mul(BigNumber.from("5"))
        .div(BigNumber.from("100"))
        .add(
          data.amounts[0]
            .mul(BigNumber.from("95"))
            .mul(BigNumber.from("1"))
            .div(BigNumber.from("100"))
            .div(BigNumber.from("24"))
        )
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );

    // Move to after vesting time
    await time.increase(24 * month);
    await buyer1.release();
    await buyer2.release();
    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    expect((await owner.lockAddress(buyer2Address)).released).to.be.equal(
      data.amounts[1]
    );
    expect(await bive.balanceOf(buyer2Address)).to.be.equal(
      (await owner.lockAddress(buyer2Address)).released
    );
    expect((await owner.lockAddress(buyer2Address)).total).to.be.equal(
      data.amounts[1]
    );

    snapshot.restore();
  });

  it("claim in vesting time - phrase 4", async function () {
    /**
     * Scenario
     * Case 1: Claim after vesting (buyer1)
     */
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1, buyer1Address } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE time
    await time.increase(2 * hour + week);

    // Move to after vesting time
    await time.increase(31 * month);
    await buyer1.release();

    expect((await owner.lockAddress(buyer1Address)).released).to.be.equal(
      data.amounts[0]
    );
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      (await owner.lockAddress(buyer1Address)).released
    );
    expect((await owner.lockAddress(buyer1Address)).total).to.be.equal(
      data.amounts[0]
    );
    await expect(buyer1.release()).to.be.revertedWith(
      "Bizverse: Nothing to release"
    );
    snapshot.restore();
  });

  it("token release after round correct", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data } = await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();

    // Move to TGE
    await time.increase(2 * hour + week);

    // Check releasable
    await owner.calcReleased(data.beneficiaries);
    expect(await owner.currentReleasable()).equal(
      parseUnits("50000000", 4)
        .mul(BigNumber.from("5"))
        .div(BigNumber.from("100"))
    );

    // Move to cliff time
    await time.increase(6 * month);

    for (let i = 2; i <= 24; i = i + 2) {
      await time.increase(2 * month);
      await owner.calcReleased(data.beneficiaries);
      console.log(
        `${i + 6} ==> ${(await owner.currentReleasable()).toNumber(10)}`
      );
      expect(await owner.currentReleasable()).equal(
        parseUnits("50000000", 4)
          .mul(BigNumber.from("5"))
          .div(BigNumber.from("100"))
          .add(
            parseUnits("50000000", 4)
              .mul(BigNumber.from("95"))
              .mul(BigNumber.from(i.toString()))
              .div(BigNumber.from("100"))
              .div(BigNumber.from("24"))
          )
      );
    }

    await snapshot.restore();
  });
});
