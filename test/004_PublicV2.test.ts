/* eslint-disable node/no-unpublished-import */
import { expect } from "chai";
import { ethers, deployments, getNamedAccounts } from "hardhat";
import { SeedFacet, BIVE, PublicV2Facet } from "../typechain";
import { time, takeSnapshot } from "@nomicfoundation/hardhat-network-helpers";
import { BigNumber, constants } from "ethers";
import { parseUnits } from "ethers/lib/utils";

const now = Math.ceil(new Date().getTime() / 1000);
const hour = 3600;
const day = hour * 24;
const week = day * 7;
const month = day * 30;

const initConfig = {
  tgeTimestamp_: now + hour,
  vestingStart_: now + hour + 6 * month,
  totalRound_: 24,
  timePerRound_: month,
  percentForTGE_: 5000,
};

const setup = deployments.createFixture(async () => {
  const { deployerTest, test1, test2, test3, test4 } = await getNamedAccounts();
  await deployments.fixture("publicV2-Dev");
  const bive = await ethers.getContract("bive-Dev", deployerTest);
  const owner = await ethers.getContract("publicV2-Dev", deployerTest);
  const buyer1 = await ethers.getContract("publicV2-Dev", test1);
  const buyer2 = await ethers.getContract("publicV2-Dev", test2);
  const buyer3 = await ethers.getContract("publicV2-Dev", test3);
  const noBuyer = await ethers.getContract("publicV2-Dev", test4);
  return {
    iPublicV2: <PublicV2Facet>owner,
    iBive: <BIVE>bive,
    ownerAddress: deployerTest,
    buyer1Address: test1,
    buyer2Address: test2,
    buyer3Address: test3,
    noBuyerAddress: test4,
    bive,
    owner,
    buyer1,
    buyer2,
    buyer3,
    noBuyer,
    data: {
      beneficiaries: [test1],
      amounts: [parseUnits("7500", 4)],
    },
  };
});

// describe("Owner Permission", function () {
//   it("init data correct", async function () {
//     const snapshot = await takeSnapshot();
//     const { bive, owner, data } = await setup();
//     expect(await owner.token()).to.be.equal(bive.address);
//     const cliffTime = (await owner.vestingStartTimestamp()).sub(
//       await owner.tgeTimestamp()
//     );
//     expect(cliffTime.toNumber(10)).to.be.equal(6 * month);
//     expect(await owner.percentForTGE()).to.be.equal(5000);
//     expect(await owner.percentForVesting()).to.be.equal(95000);
//     expect(await owner.totalRound()).to.be.equal(24);
//     expect(await owner.timePerRound()).to.be.equal(month);
//     await owner.calcReleased(data.beneficiaries);
//     expect(await owner.total()).to.be.equal(0);
//     expect(await owner.currentReleasable()).to.be.equal(0);
//     snapshot.restore();
//   });

//   it("batchLock succeed before TGE", async function () {
//     const snapshot = await takeSnapshot();
//     const { owner, buyer1, data, iSeed } = await setup();
//     await expect(owner.batchLock(data.beneficiaries, data.amounts))
//       .to.emit(iSeed, "BatchLock")
//       .withArgs(data.beneficiaries, data.amounts);
//     await expect(buyer1.batchLock(data.beneficiaries, data.amounts)).to.be
//       .reverted;
//     await owner.calcReleased(data.beneficiaries);
//     expect(await owner.total()).to.be.equal(parseUnits("50000000", 4));
//     expect(await owner.currentReleasable()).to.be.equal(0);
//     await time.increase(2 * hour);
//     await expect(
//       owner.batchLock(data.beneficiaries, data.amounts)
//     ).to.be.revertedWith("Bizverse: Action must be before TGE");
//     snapshot.restore();
//   });

//   it("deposit succeed before TGE", async function () {
//     const snapshot = await takeSnapshot();
//     const { bive, owner, buyer1, data, iSeed } = await setup();
//     await bive.approve(owner.address, constants.MaxUint256);
//     await expect(owner.deposit()).to.be.not.reverted;
//     expect(await owner.total()).to.be.equal(
//       await bive.balanceOf(owner.address)
//     );
//     await time.increase(2 * hour);
//     await expect(owner.deposit()).to.be.revertedWith(
//       "Bizverse: Action must be before TGE"
//     );
//     snapshot.restore();
//   });

//   it("withdraw succeed", async function () {
//     const snapshot = await takeSnapshot();
//     const { bive, owner, data, iSeed, noBuyerAddress } = await setup();
//     await owner.batchLock(data.beneficiaries, data.amounts);
//     await bive.approve(owner.address, constants.MaxUint256);
//     await owner.deposit();
//     await expect(owner.ownerWithdraw(data.beneficiaries[2]))
//       .to.emit(iSeed, "OwnerWithdraw")
//       .withArgs(data.beneficiaries[2]);
//     expect(await owner.total()).to.be.equal(parseUnits("5000000", 4));
//     expect((await owner.lockAddress(data.beneficiaries[2])).total).to.be.equal(
//       0
//     );
//     expect(
//       (await owner.lockAddress(data.beneficiaries[2])).released
//     ).to.be.equal(0);

//     expect(await owner.total()).to.be.equal(
//       await bive.balanceOf(owner.address)
//     );

//     await expect(owner.ownerWithdraw(noBuyerAddress)).to.be.revertedWith(
//       "Bizverse: Beneficiary not exist or claim over"
//     );

//     snapshot.restore();
//   });
// });

describe("Buyer Permission", function () {
  // it("data correct", async function () {
  //   const snapshot = await takeSnapshot();
  //   const { bive, owner, data, noBuyerAddress } = await setup();
  //   await owner.batchLock(data.beneficiaries, data.amounts);
  //   await bive.approve(owner.address, constants.MaxUint256);
  //   await owner.deposit();
  //   expect((await owner.lockAddress(data.beneficiaries[0])).total).to.be.equal(
  //     data.amounts[0]
  //   );
  //   expect(
  //     (await owner.lockAddress(data.beneficiaries[0])).released
  //   ).to.be.equal(0);
  //   expect((await owner.lockAddress(noBuyerAddress)).total).to.be.equal(0);
  //   expect((await owner.lockAddress(noBuyerAddress)).released).to.be.equal(0);
  //   snapshot.restore();
  // });

  // it("claim before tge revert", async function () {
  //   const snapshot = await takeSnapshot();
  //   const { bive, owner, data, buyer1 } = await setup();
  //   await owner.batchLock(data.beneficiaries, data.amounts);
  //   await bive.approve(owner.address, constants.MaxUint256);
  //   await owner.deposit();
  //   await expect(buyer1.release()).to.be.revertedWith(
  //     "Bizverse: Action must be after TGE"
  //   );
  //   snapshot.restore();
  // });

  // it("claim tge < x < tge + 1 week", async function () {
  //   const snapshot = await takeSnapshot();
  //   const { bive, owner, data, buyer1 } = await setup();
  //   await owner.batchLock(data.beneficiaries, data.amounts);
  //   await bive.approve(owner.address, constants.MaxUint256);
  //   await owner.deposit();
  //   await time.increase(2 * hour);
  //   await expect(buyer1.release()).to.be.revertedWith(
  //     "Bizverse: Nothing to release"
  //   );
  //   snapshot.restore();
  // });

  it("token transfered  updated after claim", async function () {
    const snapshot = await takeSnapshot();
    const { bive, owner, data, buyer1, buyer1Address, iPublicV2 } =
      await setup();
    await owner.batchLock(data.beneficiaries, data.amounts);
    await bive.approve(owner.address, constants.MaxUint256);
    await owner.deposit();
    await time.increase(2 * hour + week);

    await expect(buyer1.release())
      .to.emit(iPublicV2, "Release")
      .withArgs(
        data.beneficiaries[0],
        data.amounts[0].mul(BigNumber.from("50")).div(BigNumber.from("100"))
      );

    // Check data
    expect(await bive.balanceOf(buyer1Address)).to.be.equal(
      data.amounts[0].mul(BigNumber.from("50")).div(BigNumber.from("100"))
    );
    await snapshot.restore();
  });
});
