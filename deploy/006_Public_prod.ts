import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { diamond } = deployments;

  const { deployerBeta } = await getNamedAccounts();

  const bive = "0x130E6203F05805cd8C44093a53C7b50775eb4ca3";
  const tge = 1665648000;

  const min = 60;
  const hour = 3600;
  const day = hour * 24;
  const month = day * 30;

  const initConfig = {
    erc20Token_: bive,
    tgeTimestamp_: tge,
    timePerRound_: month,
  };

  await diamond.deploy("public-Prod", {
    from: deployerBeta,
    facets: ["PublicFacet", "OwnableFacet"],
    // execute: {
    //   contract: "PublicInit",
    //   methodName: "init",
    //   args: [initConfig],
    // },
    log: true,
    autoMine: true, // speed up deployment on local network (ganache, hardhat), no effect on live networks
  });
};
export default func;
func.tags = ["public-Prod"];
