import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { diamond } = deployments;

  const { deployerBeta } = await getNamedAccounts();

  const bive = "0x130E6203F05805cd8C44093a53C7b50775eb4ca3";
  const tge = 1665648000;

  const hour = 3600;
  const day = hour * 24;
  const month = day * 30;

  const initConfig = {
    erc20Token_: bive,
    tgeTimestamp_: tge,
    vestingStart_: tge + 6 * month,
    totalRound_: 18,
    timePerRound_: month,
    percentForTGE_: 7000,
  };

  await diamond.deploy("private-Prod", {
    from: deployerBeta,
    facets: ["PrivateFacet", "OwnableFacet"],
    // execute: {
    //   contract: "PrivateInit",
    //   methodName: "init",
    //   args: [initConfig],
    // },
    log: true,
    autoMine: true, // speed up deployment on local network (ganache, hardhat), no effect on live networks
  });
};
export default func;
func.tags = ["private-Prod"];
