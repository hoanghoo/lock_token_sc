import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { diamond } = deployments;

  const { deployerBeta } = await getNamedAccounts();

  const hour = 3600;
  const day = hour * 24;
  const month = day * 30;
  const bive = "0x130E6203F05805cd8C44093a53C7b50775eb4ca3";
  const tge = 1665648000;

  const initConfig = {
    erc20Token_: bive,
    tgeTimestamp_: tge,
    vestingStart_: tge,
    totalRound_: 60,
    timePerRound_: month,
    percentForTGE_: 0,
  };

  await diamond.deploy("staking-Prod", {
    from: deployerBeta,
    facets: ["SeedFacet", "OwnableFacet"],
    // execute: {
    //   contract: "SeedInit",
    //   methodName: "init",
    //   args: [initConfig],
    // },
    log: true,
    autoMine: true, // speed up deployment on local network (ganache, hardhat), no effect on live networks
  });
};
export default func;
func.tags = ["staking-Prod"];
