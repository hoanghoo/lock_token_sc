import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { diamond } = deployments;

  const { deployerTest } = await getNamedAccounts();

  const BIVE = await deploy("bive-Dev", {
    contract: "BIVE",
    from: deployerTest,
    args: [],
    log: true,
    autoMine: true, // speed up deployment on local network (ganache, hardhat), no effect on live networks
  });

  const now = Math.ceil(new Date().getTime() / 1000);
  const min = 60;
  const hour = 3600;
  const day = hour * 24;
  const month = day * 30;

  // const initConfig = {
  //   erc20Token_: BIVE.address,
  //   tgeTimestamp_: now + hour,
  //   totalRound_: 5,
  //   timePerRound_: month,
  //   percentForTGE_: 50000,
  // };

  const initConfig = {
    erc20Token_: "0xd8ec7641135B1CD647953fE9Eb4Ac9CE22E8Ac5b",
    tgeTimestamp_: 1664247600,
    totalRound_: 5,
    timePerRound_: 30 * min,
    percentForTGE_: 50000,
  };

  await diamond.deploy("public-Dev", {
    from: deployerTest,
    facets: ["PublicFacet", "OwnableFacet"],
    execute: {
      contract: "PublicInit",
      methodName: "init",
      args: [initConfig],
    },
    log: true,
    autoMine: true, // speed up deployment on local network (ganache, hardhat), no effect on live networks
  });
};
export default func;
func.tags = ["public-Dev"];
